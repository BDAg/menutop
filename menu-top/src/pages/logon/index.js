import React, { useState } from "react";
import { FiLogIn } from "react-icons/fi";
import { Link, useHistory } from "react-router-dom";
import { BsFillQuestionCircleFill } from "react-icons/bs"; 
import "./styles.css";

import cozinheiroImg from "../../assets/cozinheiro.png";
import logoImg from "../../assets/logo.svg";

import api from "../../services/api";

import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";

export default function Logon() {
  const [id, setId] = useState("");
  const [show, setShow] = useState(false);
  const [email, setEmail] = useState("");

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  
  const history = useHistory();

  function redirect() {
    const isLogged = !!localStorage.getItem("id");
    const type = localStorage.getItem("type");
    if (isLogged) {
      if (type === "consumer") {
        history.push("/consumer");
      } else if (type === "restaurant") {
        history.push("/profile");
      }
    }
  }

  async function handleLogin(e) {
    e.preventDefault();

    try {
      const response = await api.post("session", { id });
      localStorage.setItem("id", id);
      localStorage.setItem("name", response.data.name);
      localStorage.setItem("type", response.data.type);
      
      if (response.data.type === "consumer") {
        history.push("/consumer");
      } else if (response.data.type === "restaurant") {
        history.push("/profile");
      }
    } catch (err) {
      alert("falha no login");
    }
  }

  async function handleRememberId(e) {
    e.preventDefault();
    const data = {
      email: email
    }
    try{
      const response = await api.post("remember-id", data);
      alert( response.data.message )
      handleClose();
    } catch (err) {
      return alert(err)
    }

    
  }
  


  redirect();

  return (
    <div className="logon-container">
      <section className="form">
        <img src={logoImg} alt="logo" />

        <form onSubmit={handleLogin}>
          <input
            placeholder="Seu ID"
            value={id}
            onChange={(e) => setId(e.target.value)}
          ></input>

          <button className="button" type="submit">
            Entrar
          </button>
          <div className="back-link" onClick={handleShow}>
            <BsFillQuestionCircleFill size={16} color="#e02041" />
           Esqueci o id
          </div>
          <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
              <Modal.Title>Recuperação de ID</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <form>
                <input value={email} onChange={ e => setEmail(e.target.value)} placeholder="Digite seu email" type="email" />
              </form>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="danger" onClick={handleRememberId}>
                Recuperar ID
              </Button>
            </Modal.Footer>
          </Modal>

          <Link className="back-link" to="/register">
            <FiLogIn size={16} color="#e02041" />
            Não tenho cadastro
          </Link>
        </form>
      </section>

      <img src={cozinheiroImg} alt="cozinheiro" />
    </div>
  );
}

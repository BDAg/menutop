

import React from 'react';
import { Link , useHistory} from 'react-router-dom';
import logoImg from "../../assets/logo.svg";
import { FiArrowLeft} from "react-icons/fi";
import "./styles.css";

export default function Menu (){
    return (
        <div className="menu-container">
             <header>
                <img src={logoImg} alt="Logo"/>

                <Link button type="button" to="/menu">
                    <button>Voltar</button>
                </Link>
            </header>

            <h1>Carrinho</h1>

            <ul>
                <li>
                    <strong>Produtos:</strong>
                    <p>Produto teste </p>
                    <strong>Quantidade:</strong>
                    <p>1x</p>
                    <strong>Valor:</strong>
                    <p>R$12,00</p>


                    <Link button type="button" to="/menu">
                    <button>
                    Excluir
                    </button>
                    </Link>

                </li>

                <li>
                    <strong>Produtos:</strong>
                    <p>Produto teste </p>
                    <strong>Quantidade:</strong>
                    <p>1x</p>
                    <strong>Valor:</strong>
                    <p>R$12,00</p>


                    <Link button type="button" to="/menu">
                    <button>
                    Excluir
                    </button>
                    </Link>
                </li>

                <li>
                    <strong>Produtos:</strong>
                    <p>Produto teste </p>
                    <strong>Quantidade:</strong>
                    <p>1x</p>
                    <strong>Valor:</strong>
                    <p>R$12,00</p>


                    <Link button type="button" to="/menu">
                    <button>
                    Excluir
                    </button>
                    </Link>
                </li>


                <li>
                    <strong>Produtos:</strong>
                    <p>Produto teste </p>
                    <strong>Quantidade:</strong>
                    <p>1x</p>
                    <strong>Valor:</strong>
                    <p>R$12,00</p>


                    <Link button type="button" to="/menu">
                    <button>
                    Excluir
                    </button>
                    </Link>
                </li>



            

            </ul>
    
        </div>
    );
}

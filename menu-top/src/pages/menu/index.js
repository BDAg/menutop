import React from 'react';
import { Link , useHistory} from 'react-router-dom';
import logoImg from "../../assets/logo.svg";
import { FiArrowLeft} from "react-icons/fi";
import "./styles.css";

export default function Menu (){
    return (
        <div className="menu-container">
             <header>
                <img src={logoImg} alt="Logo"/>

                <Link button type="button" to="/consumer">
                    <button>Voltar</button>
                </Link>

                <Link button type="button" to="/cart">
                    <button1>Carrinho</button1>
                </Link>
            </header>

            <h1>Cardápio</h1>

            <ul>
                <li>
                    <strong>PRATO:</strong>
                    <p>Prato teste </p>
                    <strong>DESCRIÇÃO:</strong>
                    <p>Descrição teste </p>


                    <Link button type="button" to="/cart">
                    <button>
                    Adicionar ao carrinho
                    </button>
                    </Link>

                </li>

                <li>
                    <strong>PRATO:</strong>
                    <p>Prato teste </p>
                    <strong>DESCRIÇÃO:</strong>
                    <p>Descrição teste </p>

                    <Link button type="button" to="/cart">
                    <button>
                    Adicionar ao carrinho
                    </button>
                    </Link>
                </li>

                <li>
                    <strong>PRATO:</strong>
                    <p>Prato teste </p>
                    <strong>DESCRIÇÃO:</strong>
                    <p>Descrição teste </p>

                    <Link button type="button" to="/cart">
                    <button>
                    Adicionar ao carrinho
                    </button>
                    </Link>
                </li>


                <li>
                    <strong>PRATO:</strong>
                    <p>Prato teste </p>
                    <strong>DESCRIÇÃO:</strong>
                    <p>Descrição teste </p>

                    <Link button type="button" to="/cart">
                    <button>
                    Adicionar ao carrinho
                    </button>
                    </Link>
                </li>



            

            </ul>
    
        </div>
    );
}

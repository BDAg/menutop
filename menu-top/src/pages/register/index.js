import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { FiArrowDownLeft } from "react-icons/fi";

import { Row, Col, Form } from "react-bootstrap";

import api from "../../services/api";
import "./styles.css";

import LogoImg from "../../assets/logo.svg";

export default function Register() {
  const [type, setType] = useState("restaurant");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [whatsapp, setWhatsapp] = useState("");
  const [district, setDistrict] = useState("");
  const [address, setAddress] = useState("");
  const [number, setNumber] = useState("");
  const [cep, setCep] =useState("");
  const [city, setCity] = useState("");
  const [uf, setUf] = useState("");

  const history = useHistory();

  async function handleRegister(e) {
    e.preventDefault();
    const data = {
      type,
      name,
      email,
      whatsapp,
      district,
      address,
      number,
      cep,
      city,
      uf,
    };
    try {
      const response = await api.post("restaurants", data);

      alert(response.data.message);

      history.push("/");
    } catch (err) {
      alert("erro no cadastro, tente novamente");
    }
  }

  return (
    <div className="register-container">
      <div className="content">
        <section>
          <img src={LogoImg} alt="Menu" />

          <h1>Cadastro</h1>
          <p>
            Faça seu cadastro, entre na plataforma e escolha a comida que mata
            sua fome.
          </p>

          <Link className="back-link" to="/">
            <FiArrowDownLeft size={16} color="#e02041" />
            Tenho cadastro
          </Link>
        </section>

        <Form onSubmit={handleRegister}>
          <Form.Group>
            <Row>
              <Col>
                <select
                  onChange={(e) => setType(e.target.value)}
                  name="tipo-negocio"
                  id="tipo-negocio"
                >
                  <option value="restaurant">Restaurante</option>
                  <option value="consumer">Consumidor</option>
                </select>
              </Col>
            </Row>
            <Row>
              <Col>
                <input
                  placeholder="Nome"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                />
              </Col>
            </Row>
            <Row>
              <Col>
                <input
                  type="email"
                  placeholder="email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
              </Col>
            </Row>
            <Row>
              <Col>
                <input
                  placeholder="Whatsapp"
                  value={whatsapp}
                  onChange={(e) => setWhatsapp(e.target.value)}
                />
              </Col>
            </Row>
            <Row>
                <Col>
                    <input
                        placeholder="CEP"    
                        value={cep}
                        onChange={(e)=> setCep(e.target.value)}
                    />     
                        
                </Col>
            </Row>
            <Row>
              <Col>
                <input
                  placeholder="Cidade"
                  style={{ width: "100%" }}
                  value={city}
                  onChange={(e) => setCity(e.target.value)}
                />
              </Col>
              <Col sm={3}>
                <input
                  placeholder="UF"
                  value={uf}
                  onChange={(e) => setUf(e.target.value)}
                />
              </Col>
            </Row>
            <Row>
              <Col>
                <input
                  placeholder="Bairro"
                  value={district}
                  onChange={(e) => setDistrict(e.target.value)}
                />
              </Col>
            </Row>
            <Row>
              <Col>
                <input
                  placeholder="Endereço"
                  value={address}
                  onChange={(e) => setAddress(e.target.value)}
                />
              </Col>
              <Col sm={3}>
                <input
                  placeholder="Nº"
                  value={number}
                  onChange={(e) => setNumber(e.target.value)}
                />
              </Col>
            </Row>

            <button className="button" type="submit">
              Cadastrar
            </button>
          </Form.Group>
        </Form>
      </div>
    </div>
  );
}

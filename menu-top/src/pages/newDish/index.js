import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { FiArrowDownLeft } from "react-icons/fi";
import { uniqueId } from "lodash";

import api from "../../services/api";
import "./styles.css";

import filesize from "filesize";
import FileList from "../FileList/index";
import Upload from "../upload/index";
import LogoImg from "../../assets/logo.svg";

export default function NewDish() {
  const [dish, setNewDish] = useState("");
  const [description, setDescription] = useState("");
  const [value, setValue] = useState("");
  const [state, setState] = useState({ uploadedFiles: [] });

  const id = localStorage.getItem("id");

  const history = useHistory();

  function handleUploads(files) {
    const uploadedFiles = files.map((file) => ({
      file,
      id: uniqueId(),
      name: file.name,
      readableSize: filesize(file.size),
      preview: URL.createObjectURL(file),
      progress: 0,
      uploaded: false,
      error: false,
      url: null,
    }));

    setState({
      uploadedFiles: state.uploadedFiles.concat(uploadedFiles),
    });
  }

  function updateFile(id, data) {
    setState({
      uploadedFiles: state.uploadedFiles.map((uploadedFile) => {
        return id === uploadedFile.id
          ? { ...uploadedFile, ...data }
          : uploadedFile;
      }),
    });
  }

  async function handleNewDish(e) {
    e.preventDefault();
    const data = new FormData();
    data.append("file", state.uploadedFiles[0].file);
    data.append("dish", dish);
    data.append("description", description);
    data.append("value", value);

    try {
      if (id) {
        await api.post("/menus", data, {
          headers: {
            authorization: id,
          },
          onUploadProgress: (e) => {
            const progress = parseInt(Math.round((e.loaded * 100) / e.total));
        
            updateFile(state.uploadedFiles[0].id, {
                progress
            })
            if (progress === 100){
                setTimeout(
                    alert("item criado com sucesso!"), 2000

                )
            }
        },
        });

        history.push("profile");
      }
    } catch (err) {
      alert("tente novamente mais tarde");
    }
  }

  return (
    <div className="newdish-container">
      <div className="content">
        <section>
          <img src={LogoImg} alt="Menu" />

          <h1>Cadastrar novo prato</h1>
          <p>Descreva os ingredientes ,aromas e sabores do seu prato.</p>

          <Link className="back-link" to="/profile">
            <FiArrowDownLeft size={16} color="#e02041" />
            Voltar para home
          </Link>
        </section>

        <form action="">
          <Upload onUpload={handleUploads} />
          {!!state.uploadedFiles.length && (
            <FileList files={state.uploadedFiles} />
          )}
          <input
            value={dish}
            onChange={(e) => setNewDish(e.target.value)}
            placeholder="Nome do prato"
          />
          <textarea
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            placeholder="Descrição do prato"
          />
          <input
            value={value}
            onChange={(e) => setValue(e.target.value)}
            placeholder="Valor em reais"
          />
          <button className="button" onClick={handleNewDish} type="submit">
            Cadastrar
          </button>
        </form>
      </div>
    </div>
  );
}

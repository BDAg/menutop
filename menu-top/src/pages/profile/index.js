import React, { useEffect, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { FiPower, FiTrash2 } from "react-icons/fi";
import { ImProfile } from "react-icons/im";
import {
  ToggleButton,
  ButtonGroup,
  Modal,
  Row,
  Col,
  Image,
} from "react-bootstrap/";
import LogoImg from "../../assets/logo.svg";

import api from "../../services/api";

import "./styles.css";

export default function Profile() {
  const [dishes, setDishes] = useState([]);
  const [drinks, setDrinks] = useState([]);
  const [lgShow, setLgShow] = useState(false);
  const [email, setEmail] = useState("");
  const [whatsapp, setWhatsapp] = useState("");
  const [city, setCity] = useState("");
  const [newName, setNewName] = useState("");
  const [cep, setCep] = useState("");
  const [uf, setUf] = useState("");
  const [address, setAddress] = useState("");
  const [number, setNumber] = useState("");
  const [district, setDisctrict] = useState("");
  const [open, setOpen] = useState("00:00");
  const [close, setClose] = useState("00:00");

  const history = useHistory();

  const name = localStorage.getItem("name");
  const id = localStorage.getItem("id");

  useEffect(() => {
    api
      .get("/menu", {
        headers: {
          authorization: id,
        },
      })
      .then((response) => {
        setDishes(response.data);
      });
  }, [id]);

  async function handleDeleteMenu(ide) {
    try {
      await api.delete(`/menu/${ide}`, {
        headers: {
          authorization: id,
        },
      });
      setDishes(dishes.filter((dishes) => dishes.id_menu !== ide));
    } catch (err) {
      alert("Erro ao deletar prato, tente novamente mais tarde.");
    }
  }

  async function handleDeleteDrink(ide) {
    try {
      await api.delete(`/menu-drink/${ide}`, {
        headers: {
          authorization: id,
        },
      });
      setDrinks(drinks.filter((drinks) => drinks.id_drink !== ide));
    } catch (err) {
      alert("Erro ao deletar prato, tente novamente mais tarde.");
    }
  }

  async function handlePutProfile() {
    localStorage.setItem("name", newName);
    const data = {
      name: newName,
      email,
      whatsapp,
      city,
      newName,
      cep,
      uf,
      address,
      number,
      district,
      open,
      close
    };
    try {
      await api
        .put("/restaurants", data, {
          headers: {
            authorization: id,
          },
        })
        .then((response) => {
          alert("Perfil alterado com sucesso!");
        });
    } catch (err) {
      console.log(err);
    }
  }

  async function handleOpenProfile() {
    const id = localStorage.getItem("id");
    const response = await api.post("session", { id });
    const data = response.data;
    setLgShow(true);
    setNewName(data.name);
    setWhatsapp(data.whatsapp);
    setCity(data.city);
    setEmail(data.email);
    setCep(data.cep);
    setUf(data.uf);
    setAddress(data.address);
    setNumber(data.number);
    setDisctrict(data.district);
    setOpen(data.open);
    setClose(data.close);
  }

  function handleLogaut() {
    localStorage.clear();

    history.push("/");
  }

  const [radioValue, setRadioValue] = useState("1");

  async function handleBebidas(id) {
    setRadioValue(id);
    if (id === "2") {
      if (!drinks.length) {
        try {
          await api
            .get("/menu-drink", {
              headers: {
                authorization: localStorage.getItem("id"),
              },
            })
            .then((response) => {
              setDrinks(response.data);
            });
        } catch (err) {
          alert("erro ao carregar");
        }
      }
    }
  }

  const radios = [
    { name: "Menu", value: "1" },
    { name: "Bebidas", value: "2" },
  ];
  return (
    <div className="profile-container">
      <header>
        <img src={LogoImg} alt="Logo" />
        <span>Bem vindo, {name}</span>
        <Link className="button" to="/newdish">
          Cadastrar novo prato
        </Link>
        <Link className="button" to="/newdrink">
          Cadastrar nova bebida
        </Link>
        <button onClick={handleOpenProfile} type="button">
          <ImProfile size={18} collor="#02041" />
        </button>
        <Modal
          animation={false}
          size="lg"
          show={lgShow}
          onHide={() => setLgShow(false)}
        >
          <Modal.Header closeButton>
            <Modal.Title>Perfil</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form>
              <Row>
                <Col>
                  <span>Abertura</span>
                  <input
                    value={open}
                    onChange={(e) => {
                      setOpen(e.target.value);
                      console.log(e.target.value);
                    }}
                    type="time"
                  ></input>
                </Col>
                <Col>
                  <span>Fechamento</span>
                  <input
                    value={close}
                    onChange={(e) => setClose(e.target.value)}
                    type="time"
                  ></input>
                </Col>
              </Row>
              <Row>
                <Col>
                  <input
                    value={newName}
                    onChange={(e) => setNewName(e.target.value)}
                    placeholder="Name"
                  ></input>
                </Col>
              </Row>
              <Row>
                <Col>
                  <input
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    placeholder="Email"
                  ></input>
                </Col>
              </Row>
              <Row>
                <Col>
                  <input
                    value={whatsapp}
                    onChange={(e) => setWhatsapp(e.target.value)}
                    placeholder="Whatsapp"
                  ></input>
                </Col>
              </Row>
              <Row>
                <Col>
                  <input
                    value={cep}
                    onChange={(e) => setCep(e.target.value)}
                    placeholder="Cep"
                  ></input>
                </Col>
              </Row>
              <Row>
                <Col>
                  <input
                    value={city}
                    onChange={(e) => setCity(e.target.value)}
                    placeholder="City"
                  ></input>
                </Col>
                <Col sm={3}>
                  <input
                    value={uf}
                    onChange={(e) => setUf(e.target.value)}
                    placeholder="Uf"
                  ></input>
                </Col>
              </Row>
              <Row>
                <Col>
                  <input
                    value={district}
                    onChange={(e) => setDisctrict(e.target.value)}
                    placeholder="Bairro"
                  ></input>
                </Col>
              </Row>
              <Row>
                <Col>
                  <input
                    value={address}
                    onChange={(e) => setAddress(e.target.value)}
                    placeholder="Endereço"
                  ></input>
                </Col>
                <Col sm={3}>
                  <input
                    value={number}
                    onChange={(e) => setNumber(e.target.value)}
                    placeholder="Nº"
                  ></input>
                </Col>
              </Row>
              <Row>
                <Col>
                  <button onClick={handlePutProfile} className="button">
                    Salvar Perfil
                  </button>
                </Col>
              </Row>
            </form>
          </Modal.Body>
        </Modal>
        <button onClick={handleLogaut} type="button">
          <FiPower size={18} collor="#02041" />
        </button>{" "}
      </header>

      <h1>Pratos cadastrados</h1>
      <Row>
        <ButtonGroup toggle>
          {radios.map((radio, idx) => (
            <ToggleButton
              key={idx}
              type="radio"
              variant="secondary"
              name="radio"
              value={radio.value}
              checked={radioValue === radio.value}
              onChange={(e) => handleBebidas(e.currentTarget.value)}
            >
              {radio.name}
            </ToggleButton>
          ))}
        </ButtonGroup>
      </Row>
      {radioValue === "1" && (
        <ul>
          {dishes.map((dishes, index) => (
            <li key={index}>
              <Row>
                <Col>
                  <Image
                    height={167}
                    width={300}
                    src={"http://localhost:3333/uploads/" + dishes.img}
                  ></Image>
                </Col>
              </Row>

              <strong>Prato:</strong>

              <p>{dishes.dish}</p>

              <strong>DESCRIÇÃO:</strong>
              <p>{dishes.description}</p>

              <strong>VALOR:</strong>
              <p>
                {Intl.NumberFormat("pt-BR", {
                  style: "currency",
                  currency: "BRL",
                }).format(dishes.value)}
              </p>

              <button
                onClick={() => handleDeleteMenu(dishes.id_menu)}
                type="button"
              >
                <FiTrash2 size={20} color="#a8a8b3" />
              </button>
            </li>
          ))}
        </ul>
      )}
      {radioValue === "2" && (
        <ul>
          {drinks.map((drinks, index) => (
            <li key={index}>
              <Row>
                <Col>
                  <Image
                    height={167}
                    width={300}
                    src={"http://localhost:3333/uploads/" + drinks.img}
                  ></Image>
                </Col>
              </Row>

              <strong>Prato:</strong>

              <p>{drinks.drink}</p>

              <strong>DESCRIÇÃO:</strong>
              <p>{drinks.description}</p>

              <strong>VALOR:</strong>
              <p>
                {Intl.NumberFormat("pt-BR", {
                  style: "currency",
                  currency: "BRL",
                }).format(drinks.value)}
              </p>

              <button
                onClick={() => handleDeleteDrink(drinks.id_drink)}
                type="button"
              >
                <FiTrash2 size={20} color="#a8a8b3" />
              </button>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
}

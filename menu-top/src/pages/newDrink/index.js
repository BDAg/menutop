import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { FiArrowDownLeft } from "react-icons/fi";
import { uniqueId } from "lodash";

import api from "../../services/api";
import "./styles.css";

import filesize from "filesize";
import FileList from "../FileList/index";
import Upload from "../upload/index";
import LogoImg from "../../assets/logo.svg";

export default function NewDrink() {
  const [drink, setNewDrink] = useState("");
  const [description, setDescription] = useState("");
  const [value, setValue] = useState("");
  const [state, setState] = useState({ uploadedFiles: [] });

  const id = localStorage.getItem("id");

  const history = useHistory();

  function handleUploads(files) {
    const uploadedFiles = files.map((file) => ({
      file,
      id: uniqueId(),
      name: file.name,
      readableSize: filesize(file.size),
      preview: URL.createObjectURL(file),
      progress: 0,
      uploaded: false,
      error: false,
      url: null,
    }));

    setState({
      uploadedFiles: state.uploadedFiles.concat(uploadedFiles),
    });
  }


  async function handleNewDrink(e) {
    e.preventDefault();
    const data = new FormData();
    data.append("file", state.uploadedFiles[0].file);
    data.append("drink", drink);
    data.append("description", description);
    data.append("value", value);

    try {
      if (id) {
        await api.post("/menus-drink", data, {
          headers: {
            authorization: id,
          },
        });
        alert('item criado com sucesso')
        history.push("profile");
      }
    } catch (err) {
      alert("tente novamente mais tarde");
    }
  }

  return (
    <div className="newdish-container">
      <div className="content">
        <section>
          <img src={LogoImg} alt="Menu" />

          <h1>Cadastrar nova bebida</h1>
          <p>Descreva os ingredientes ,aromas e sabores da sua bebida.</p>

          <Link className="back-link" to="/profile">
            <FiArrowDownLeft size={16} color="#e02041" />
            Voltar para home
          </Link>
        </section>

        <form action="">
          <Upload onUpload={handleUploads} />
          {!!state.uploadedFiles.length && (
            <FileList files={state.uploadedFiles} />
          )}
          <input
            value={drink}
            onChange={(e) => setNewDrink(e.target.value)}
            placeholder="Nome da bebida"
          />
          <textarea
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            placeholder="Descrição da bebida"
          />
          <input
            value={value}
            onChange={(e) => setValue(e.target.value)}
            placeholder="Valor em reais"
          />
          <button className="button" onClick={handleNewDrink} type="submit">
            Cadastrar
          </button>
        </form>
      </div>
    </div>
  );
}

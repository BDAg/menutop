import React from 'react';
import { Link , useHistory} from 'react-router-dom';

import logoImg from "../../assets/logo.svg";
import { FiPower} from "react-icons/fi";

import "./styles.css";


export default function Consumer() {
    const history = useHistory();
    function handleLogout() {
        localStorage.clear();
    
        history.push("/");
      }

    return (
        <div className="consumer-container">
            <header>
                <img src={logoImg} alt="Logo" />
                <span>Bem vindo, Luis</span> 
                <button type="button" onClick={handleLogout}>
                    <FiPower size={18} color="E02041"/>
                </button>
            </header>

            <h1>Restaurantes</h1>

            <ul>
                <li>
                    <strong>NOME:</strong>
                    <p>Restaurante teste </p>
                    <strong>ENDEREÇO:</strong>
                    <p>Endereço teste </p>

                    <Link button type="button" to="/menu">
                    <button>
                    Ver cardápio
                    </button>
                    </Link>
                </li>


                <li>
                    <strong>NOME:</strong>
                    <p>Restaurante teste </p>
                    <strong>ENDEREÇO:</strong>
                    <p>Endereço teste </p>

                    <Link button type="button" to="/menu">
                    <button>
                    Ver cardápio
                    </button>
                    </Link>
                </li>


                <li>
                    <strong>NOME:</strong>
                    <p>Restaurante teste </p>
                    <strong>ENDEREÇO:</strong>
                    <p>Endereço teste </p>

                    <Link button type="button" to="/menu">
                    <button>
                    Ver cardápio
                    </button>
                    </Link>
                </li>


                <li>
                    <strong>NOME:</strong>
                    <p>Restaurante teste </p>
                    <strong>ENDEREÇO:</strong>
                    <p>Endereço teste </p>

                    <Link button type="button" to="/menu">
                    <button>
                    Ver cardápio
                    </button>
                    </Link>
                </li>

            </ul>
        </div>
        
    )
}
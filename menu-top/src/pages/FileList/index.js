import React from "react";
import "react-circular-progressbar/dist/styles.css";

import { Container, FileInfo, Preview } from "./styles";

import { MdCheckCircle, MdLink, MdError } from "react-icons/md";

export default function FileList({ files }) {
  return (
    <Container>
      {files.map((uploadedFile, index) => (
        <li key={index}>
          <FileInfo>
            <Preview src={uploadedFile.preview} />
            <div>
              <strong>{uploadedFile.name}</strong>
              <span>
                {uploadedFile.readableSize}{" "}
                {!!uploadedFile.url && (
                  <button onClick={() => {}}>Excluir</button>
                )}
              </span>
            </div>
          </FileInfo>

          <div>
            {!uploadedFile.uploaded && !uploadedFile.error && (
                            <MdCheckCircle size={24} color="#78e5d5" />

            )}
            {uploadedFile.url && (
              <a
                href="~/home/gui/projetos/menutop/backend/uploads/f1966a475ade212d2cdc3ff688ecb086-comida.jpeg"
                target="_blank"
                rel="noopener noreferrer"
              >
                <MdLink style={{ marginRigth: 8 }} size={24} collor="#222" />
              </a>
            )}
            {uploadedFile.error && <MdError size={24} collor="#e57878" />}
          </div>
        </li>
      ))}
    </Container>
  );
}

import React from 'react';
import { BrowserRouter , Route, Switch } from 'react-router-dom';

import Logon from './pages/Logon';
import NewDish from './pages/NewDish';
import Profile from './pages/Profile';
import Register from './pages/Register';
import Consumer from './pages/Consumer';
import Menu from './pages/menu';
import NewDrink from './pages/newDrink';
import PrivateRoute from './PrivateRoute';


export default function Routes() {
    return(
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={Logon} />
                <Route path="/register" component={Register} />
                <PrivateRoute path="/profile" component={Profile} />
                <PrivateRoute path="/newdish" component={NewDish} />
                <PrivateRoute path="/newdrink" component={NewDrink} />
                <PrivateRoute path="/consumer" component={Consumer} />  
                <Route path="/menu" component={Menu} />        
            </Switch>
        </BrowserRouter>
    )
}
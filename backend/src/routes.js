const { request, response } = require('express');
const express = require('express');
const { celebrate, Segments, Joi } = require('celebrate');

const RestaurantController = require('./controllers/RestaurantController');
const MenuController = require('./controllers/MenuController');
const ProfileController = require('./controllers/ProfileController');
const SessionController = require('./controllers/SessionController');
const DrinkController = require('./controllers/DrinkController');
const ComandController = require('./controllers/ComandController');
const multer = require('multer');
const multerConfig = require('./config/multer');
const path = require ('path')

const routes = express.Router();

routes.use('/uploads', express.static(path.join(__dirname, 'uploads')));
routes.post('/session', SessionController.create);
routes.post('/comand', ComandController.sendcomand);
routes.get('/restaurants', RestaurantController.index);
routes.post('/restaurants', celebrate({
    [Segments.BODY]: Joi.object().keys({
        type: Joi.string().required(),
        name: Joi.string().required(),
        email: Joi.string().required().email(),
        whatsapp: Joi.string().required().min(10).max(11).regex(/^\d+$/),
        district: Joi.string().required(),
        address: Joi.string().required(),
        number: Joi.string().required().regex(/^\d+$/),
        cep: Joi.string().required().length(8).regex(/^\d+$/),
        city: Joi.string().required(),
        uf: Joi.string().required().length(2),
    })
}), RestaurantController.create);
routes.post('/remember-id', RestaurantController.RememberId);
routes.put('/restaurants', RestaurantController.update);

routes.get('/profile', ProfileController.index);

routes.post('/menus', MenuController.createDish);
routes.get('/menus', MenuController.index);
routes.delete('/menus/:id', MenuController.delete);
routes.get('/menus', MenuController.list);
routes.post('/menus',multer(multerConfig).single('file'),MenuController.createDish);
routes.get('/menu', MenuController.index);
routes.delete('/menu/:id', MenuController.delete);


routes.post('/menus-drink', multer(multerConfig).single('file'), DrinkController.createDrink);
routes.get('/menu-drink', DrinkController.indexDrink);
routes.delete('/menu-drink/:id', DrinkController.delete);

module.exports = routes;


const { create } = require("./RestaurantController");
const connection = require('../database/connection');

module.exports = {
    async create(request, response) {
        const {id} = request.body;

        const restaurant = await connection('restaurants')
            .where('id', id)
            .select('*')
            .first();

        if (!restaurant) {
            return response.status(400).json({error: 'No restaurant found with this ID'});
        }

        return response.json(restaurant);
    }
}
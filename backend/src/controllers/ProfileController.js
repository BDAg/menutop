const connection = require("../database/connection");
const { index } = require("./RestaurantController");

module.exports = {
    async index (request, response) {
        const restaurant_id = request.headers.authorization;
        
        const menus = await await connection('menus')
            .where('restaurants_id', restaurant_id)
            .select('*');
        
        return response.json(menus)
    }
}
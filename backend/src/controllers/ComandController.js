const connection = require("../database/connection");
const nodemailer = require("nodemailer");

function sendMail(text, subject, to) {
  const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    // port: 587,
    secure: "ssl", // true for 465, false for other ports
    auth: {
      user: "guipaglioni@gmail.com",
      pass: "gui215487",
    },
  });

  const mailOptions = {
    from: "guipaglioni@gmail.com",
    to: to,
    subject: subject,
    text: text,
  };

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log("Email enviado: " + info.response);
    }
  });
}

module.exports = {

  async sendcomand(request, response) {
    const {comand, email , subject} = request.body;
    sendMail (comand, subject, email); 
    return "ok";
  }

};

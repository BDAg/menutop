const connection = require("../database/connection");

module.exports = {
  async indexDrink(request, response) {
    const restaurant_id = request.headers.authorization;
    const menus = await connection("drinks")
      .innerJoin("restaurants", "drinks.restaurants_id", "=", "restaurants .id")
      .where("restaurants.id", restaurant_id)
      .select(
        "drinks.id as id_drink",
        "drinks.drink",
        "drinks.img",
        "drinks.description",
        "drinks.value",
        "drinks.restaurants_id"
      );
    return response.json(menus);
  },

  async delete(request, response) {
    const { id } = request.params;
    const restaurant_id = request.headers.authorization;

    const menu = await connection("drinks")
      .where("id", id)
      .select("restaurants_id")
      .first();

    if (menu.restaurants_id !== restaurant_id) {
      return response.status(401).json({ error: "Operation not permitted." });
    }

    await connection("drinks").where("id", id).delete();

    return response.status(204).send();
  },

  async createDrink(request, response) {
    let img;
    const { drink, description, value } = request.body;
    const restaurants_id = request.headers.authorization;
    if (request.file) {
      img = request.file.filename;
    }

    const [id] = await connection("drinks").insert({
      drink,
      img,
      description,
      value,
      restaurants_id,
    });

    return response.json({ id });
  },
};

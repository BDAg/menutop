const conection = require("../database/connection");
const crypto = require("crypto");

const nodemailer = require("nodemailer");
const { json } = require("express");
const { update } = require("../database/connection");

function sendMail(text, subject, to) {
  const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    // port: 587,
    secure: "ssl", // true for 465, false for other ports
    auth: {
      user: "seu email",
      pass: "senha do seu email",
    },
  });

  const mailOptions = {
    from: "seu email",
    to: to,
    subject: subject,
    text: text,
  };

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log("Email enviado: " + info.response);
    }
  });
}

module.exports = {
  async index(request, response) {
    const restaurants = await conection("restaurants").select("*");
    return response.json(restaurants);
  },

  async RememberId(request, response) {
    const { email } = request.body;
    const restaurants = await conection("restaurants")
      .select("*")
      .where("restaurants.email", "=", email);

    if (restaurants.length !== 0) {
      const text = "Seu id é" + restaurants[0]["id"];
      const subject = "Recuperação de id";
      sendMail(text, subject, email);
      return response.send({ message: "Email enviado com sucesso!" });
    } else if (restaurants.length === 0) {
      return response.send({ message: "Email não cadastrado!" });
    }
  },

  async update(request, response) {
    const {
      name,
      email,
      whatsapp,
      district,
      address,
      number,
      cep,
      city,
      uf,
      open,
      close
    } = request.body;
    const id = request.headers.authorization;
    const restaurant = await conection("restaurants")
      .where({ id: id })
      .update({
        name: name,
        email: email,
        whatsapp: whatsapp,
        district: district,
        address: address,
        number: number,
        cep: cep,
        city: city,
        uf: uf,
        open: open,
        close: close
      })
      .then((data) => {
        console.log(data);
      });
    return restaurant;
  },

  async create(request, response) {
    const {
      type,
      name,
      email,
      whatsapp,
      district,
      address,
      number,
      cep,
      city,
      uf,
    } = request.body;
    const restaurant = await conection("restaurants")
      .select("*")
      .where("restaurants.email", "=", email);

    if (restaurant.length != 0) {
      const text =
        "Ola " +
        restaurant[0]["name"] +
        " seja bem vindo de volta ao menu top seu id é " +
        restaurant[0]["id"];
      const subject = "Recuperação de Id";
      const to = restaurant[0]["email"];

      sendMail(text, subject, to);

      return response.json({
        message:
          "Seu email já esta cadastrado e seu id foi enviado para sua caixa de entrada.",
      });
    } else {
      const id = crypto.randomBytes(4).toString("HEX");

      const text = "Seu id é: " + id;

      const subject = "Cadastro Menu-top";
      await conection("restaurants").insert({
        id,
        type,
        name,
        email,
        whatsapp,
        district,
        address,
        number,
        cep,
        city,
        uf,
      });

      sendMail(text, subject, email);

      return response.json({
        id,
        name,
        message: "Ola " + name + "seu id é " + id,
      });
    }
  },
};

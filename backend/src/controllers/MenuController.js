const connection = require("../database/connection");

module.exports = {
  async index(request, response) {
    const restaurant_id = request.headers.authorization;
    const menus = await connection("menus")
      .innerJoin("restaurants", "menus.restaurants_id", "=", "restaurants .id")
      .where("restaurants .id", restaurant_id)
      .select(
        "menus.id as id_menu",
        "menus.dish",
        "menus.img",
        "menus.description",
        "menus.value",
        "menus.restaurants_id",
        "restaurants.*"
      );

    return response.json(menus);
  },

  async list(request, response) {
    const menus = await connection("menus");

    return response.json(menus);
  },

  async createDish(request, response) {
    const { dish, description, value } = request.body;
    const restaurants_id = request.headers.authorization;
    const img = request.file.filename;

    const [id] = await connection("menus").insert({
      dish,
      img,
      description,
      value,
      restaurants_id,
    });

    return response.json({ id });
  },

  async delete(request, response) {
    const { id } = request.params;
    const restaurant_id = request.headers.authorization;

    const menu = await connection("menus")
      .where("id", id)
      .select("restaurants_id")
      .first();

    if (menu.restaurants_id !== restaurant_id) {
      return response.status(401).json({ error: "Operation not permitted." });
    }

    await connection("menus").where("id", id).delete();

    return response.status(204).send();
  },
};


exports.up = function(knex) {
    return knex.schema.createTable('restaurants', function (table) {
        table.string('id').primary();
        table.string('type').notNullable();
        table.string('name').notNullable();
        table.string('email').notNullable();
        table.string('whatsapp').notNullable();
        table.string('district').notNullable();
        table.string('address').notNullable();
        table.string('number').notNullable();
        table.string('cep').notNullable();
        table.string('city').notNullable();
        table.string('uf', 2).notNullable(); 
        table.string('open');
        table.string('close')
      });
};

exports.down = function(knex) {
  return knex.schema.dropTable('restaurants');
};

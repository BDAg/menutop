
exports.up = function(knex) {
    return knex.schema.createTable('menus', function (table) {
        table.increments();
        
        table.string('dish').notNullable();
        table.string('img');
        table.string('description').notNullable();
        table.decimal('value').notNullable();
        
        table.string('restaurants_id').notNullable();
        table.foreign('restaurants_id').references('id').inTable('restaurants')
      });
};

exports.down = function(knex) {
  return knex.schema.dropTable('menus');
};

<div align="center"><h1>Menutop</h1></div>
O MenuTop Será um Projeto intuitivo, um site de uma pizzaria, no qual o usuário poderá fazer pedidos rápidos e fáceis, somente colocando o numero de telefone. Isso facilitará no processo dos pedidos e vendas oferecendo mais lucros e melhor satisfação do cliente.
<hr/>

## Wiki
[Wiki do projeto](https://gitlab.com/BDAg/menutop/-/wikis/home)
<hr/>

## Equipe

[Daniel Vieira Mendes](https://gitlab.com/BDAg/menutop/-/wikis/Daniel-Vieira-Mendes)
<br>
[Guilherme Oliveira Paglioni](https://gitlab.com/BDAg/menutop/-/wikis/Guilherme-Oliveira-Paglioni)
<br>
[Helena Mattos Sotrate](https://gitlab.com/BDAg/menutop/-/wikis/Helena-Mattos-Sotrate)
<br>
[Janaina Pinheiro Gonçalez](https://gitlab.com/BDAg/menutop/-/wikis/Janaina-Pinheiro-Gon%C3%A7alez)
<br>
[Maria Eduarda Silva de Oliveira](https://gitlab.com/BDAg/menutop/-/wikis/Maria-Eduarda-de-Oliveira)
<hr/>

## Instalação

<p>Primeiramente baixe nosso projeto e descompacte ele numa pasta, recomendamos fazê-lo através do git.</p>
<p>Esta aplicação foi feita utilizando Node.js, então precisamos instalá-lo.</p>

#### Node

<p>Baixe o node através do [site](https://nodejs.org/pt-br/download/) (recomendo a versão LTS), ou através de um gerenciador de pacotes.<br><br>
Se estiver no linux (dpkg): <br>

> sudo apt install nodejs
> sudo apt install npm

Se estiver no mac (homebrew):

> brew install node

Se estiver no windows (chocolatey):

> choco install node lts

</p>

#### Git (opcional)

<p>Instale o git seguindo este [tutorial](https://bigdataagronegocio.wordpress.com/2017/03/11/tutorial-de-instalacao-e-configuracao-do-gitlab-no-windows/).<br> Após  Instalação e configuração, utilize o seguinte comando para baixar nosso projeto: </p> 

> git clone git@gitlab.com:BDAg/menutop.git

#### Dependências do Node.js

<p>Na pasta raiz da aplicação, execute este comando para instalar as dependências do Node.js: </p>

> cd backend && npm install

#### Dependências do React.js

<p>Na pasta raiz da aplicação, execute este comando para instalar as dependências do React.js: </p>

> cd menu-top && npm install

#### Criando migrações do banco

<p>Na pasta raiz da aplicação, execute este comando para fazer as migrações das tabelas do banco de dados através do knex: </p>

> cd backend && npx knex migrate:latest

#### Executando backend e frontend

<p>Execute o seguinte comando nas pastas 'backend' e 'menu-top' para iniciar a aplicação e deixe-os rodando (necessita duas abas no terminal): </p>

> npm start

